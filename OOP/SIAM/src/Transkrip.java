/*KELAS TRANSKRIP MEMILIKI ATRIBUT NILAI*/

public class Transkrip {
    private Mahasiswa mhs;
    private Kursus kursus;
    private int nilai;

    public Transkrip(Mahasiswa mhs, Kursus kursus, int nilai) {
        this.mhs = mhs;
        this.kursus = kursus;
        this.nilai = nilai;
        mhs.addTranscript(this);
    }

    public Mahasiswa getMahasiswa() {
        return mhs;
    }

    public Kursus getKursus() {
        return kursus;
    }

    public int getNilai() {
        return nilai;
    }

    public void printInfo() {
        System.out.println(kursus.getNama() + ": " + nilai);
    }
}
